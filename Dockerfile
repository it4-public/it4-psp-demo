# Build stage
FROM golang:1.21.0 AS build-stage

WORKDIR /app

# Copy All folders and files into app
COPY /service/psp/ ./service/psp/

# Copy go.mod and go.sum to the working directory
COPY go.mod ./
RUN go mod download

# Copy the rest of the source files
COPY . ./

# Navigate to the correct directory to build
WORKDIR /app/service/psp/cmd/http

# Build the Go application
RUN CGO_ENABLED=0 GOOS=linux go build -o main .

# Test stage (You can uncomment this stage if you want to run tests)
# FROM build-stage AS run-test-stage
# RUN go test -v ./...

# Production stage
# Production stage
FROM alpine:latest AS production-stage

WORKDIR /app

# Copy the built binary from the build stage
COPY --from=build-stage /app/service/psp/cmd/http .

# Expose the port your application listens on
EXPOSE 8780

# Set the entry point for your application
ENTRYPOINT ["./main"]
