package main

import (
	"log"
	"net/http"
	http_handler "payment_service_platform/service/psp/handler/http"
)

func main() {

	payment := http_handler.Payment{}
	creditCard := http_handler.CreditCard{}

	// Create the handler for processing credit card transactions.
	handler := payment.Process(creditCard)

	// Apply the logging middleware to the handler.
	loggingHandler := http_handler.LoggingMiddleware(http.HandlerFunc(handler))

	// Use the logging handler for the "/transaction" endpoint.
	http.Handle("/transaction", loggingHandler)

	// Start the HTTP server.
	log.Fatal(http.ListenAndServe(":8780", nil))
}
