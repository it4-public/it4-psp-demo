package http_handler

import (
	"encoding/json"
	"log"
	"net/http"
	"payment_service_platform/service/psp/util"
)

// inMemoryStorage is a data store using a map
var inMemoryStorage = map[string]Transaction{}

// PaymentMethod defines an interface for processing payments
type PaymentMethod interface {
	ProcessPayment() func(http.ResponseWriter, *http.Request)
}

// Payment represents a payment processor that can use different methods
type Payment struct {
}

// CreditCard is a payment method implementing the PaymentMethod interface
type CreditCard struct {
}

// Process returns the HTTP handler function for a given payment method
func (p Payment) Process(pm PaymentMethod) func(http.ResponseWriter, *http.Request) {
	return pm.ProcessPayment()
}

// ProcessPayment returns the HTTP handler function for credit card payments.
func (cc CreditCard) ProcessPayment() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {

			util.FailureResponse(w, http.StatusMethodNotAllowed, "failed", "", nil)
			return
		}

		transaction := Transaction{}

		decoder := json.NewDecoder(r.Body)

		err := decoder.Decode(&transaction)
		if err != nil {
			util.FailureResponse(w, http.StatusInternalServerError, "failed", err.Error(), nil)
			return
		}

		switch {
		case transaction.CardDetails.Number == "":
			{
				util.FailureResponse(w, http.StatusBadRequest, "failed", "enter valid card number", nil)
				return
			}

		case transaction.CardDetails.Expiry == "":
			{
				util.FailureResponse(w, http.StatusBadRequest, "failed", "enter valid card expiry date", nil)
				return
			}

		case transaction.CardDetails.Cvv == "":
			{
				util.FailureResponse(w, http.StatusBadRequest, "failed", "enter valid cvv/cvc number", nil)
				return
			}
		}

		//CheckExpiredDate checks whether card date is expired or not
		_, errs := util.CheckExpiredDate(transaction.CardDetails.Expiry)
		if errs != nil {
			util.FailureResponse(w, errs.StatusCode, errs.Message, errs.Error, nil)
			return
		}

		//ValidateCardNumber validates cards using Luhn's algorithm
		ok, card_err := util.ValidateCardNumber(transaction.CardDetails.Number)
		if !ok {
			util.FailureResponse(w, card_err.StatusCode, card_err.Message, card_err.Error, nil)
			return
		}

		//generate random transaction id
		transaction.TransactionID = util.GenerateTransactionID()

		// Mask card number for storage
		transaction.CardDetails.Number = util.MaskCardNumber(transaction.CardDetails.Number)
		transaction.Status = "Pending"

		inMemoryStorage[transaction.TransactionID] = transaction

		if Acquirer(transaction) {
			transaction.Status = "Approved"
		} else {
			transaction.Status = "Denied"
		}

		util.SuccessResponse(w, http.StatusOK, "success", transaction)
	}

}

// Acquirer updates the transaction status to 'Approved' or 'Denied' based on last digit of card number.
// approved for even, denied for odd number

func Acquirer(transaction Transaction) bool {
	// Approve even last digit, Deny odd
	return transaction.CardDetails.Number[len(transaction.CardDetails.Number)-1]%2 == 0
}

// LoggingMiddleware logs request url and status code to the terminal
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		lrw := &loggingResponseWriter{ResponseWriter: w}

		log.Printf("Request URL: %s", r.URL.String())

		next.ServeHTTP(lrw, r)

		log.Printf("Status Code: %d", lrw.statusCode)
	})
}
