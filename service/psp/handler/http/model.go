package http_handler

import "net/http"

type Transaction struct {
	TransactionID string      `json:"transactionId"`
	CardDetails   CardDetails `json:"cardDetails" `
	Status        string      `json:"status"`
	Amount        float64     `json:"amount"`
	Currency      string      `json:"currency"`
	MerchantID    string      `json:"merchantId"`
}

type CardDetails struct {
	Number string `json:"number" required:"true"`
	Cvv    string `json:"cvv" required:"true"`
	Expiry string `json:"expiry" required:"true"`
}

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}
