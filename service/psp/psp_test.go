package service

import (
	"encoding/json"
	"fmt"
	"net/http/httptest"
	http_handler "payment_service_platform/service/psp/handler/http"
	"payment_service_platform/service/psp/util"
	"strings"
	"testing"
)

func TestProcessPayment(t *testing.T) {
	testCases := []struct {
		name         string
		requestBody  http_handler.Transaction
		method       string
		expectedBody struct {
			Message    string
			StatusCode int
			Status     string
			MaskedCard string
			Error      string
			MerchatId  string
		}
	}{
		{
			name:   "Valid Transaction",
			method: "POST",
			requestBody: http_handler.Transaction{
				CardDetails: http_handler.CardDetails{
					Number: "5011054488597827",
					Expiry: "05/24",
					Cvv:    "231",
				},
				MerchantID: "merchant-4564",
			},
			expectedBody: struct {
				Message    string
				StatusCode int
				Status     string
				MaskedCard string
				Error      string
				MerchatId  string
			}{
				Message:    "successfull",
				StatusCode: 200,
				Status:     "Denied",
				MaskedCard: "************7827",
				MerchatId:  "merchant-4564",
				Error:      "",
			},
		},
		{
			name:   "Invalid Card Number",
			method: "POST",
			requestBody: http_handler.Transaction{
				CardDetails: http_handler.CardDetails{
					Number: "4539 3195 0343 6468",
					Expiry: "05/24",
					Cvv:    "231",
				},
			},
			expectedBody: struct {
				Message    string
				StatusCode int
				Status     string
				MaskedCard string

				Error     string
				MerchatId string
			}{
				Message:    "failed",
				StatusCode: 400,
				Error:      "invalid card number",
				MerchatId:  "merchant-4564",
			},
		},
		{
			name:   "Expired Card",
			method: "POST",
			requestBody: http_handler.Transaction{
				CardDetails: http_handler.CardDetails{
					Number: "4539 3195 0343 6467",
					Expiry: "01/24",
					Cvv:    "231",
				},
			},
			expectedBody: struct {
				Message    string
				StatusCode int
				Status     string
				MaskedCard string
				Error      string
				MerchatId  string
			}{
				Message:    "card is already expired",
				StatusCode: 400,

				Error:     "",
				MerchatId: "merchant-4564",
			},
		},
		{
			name:   "Missing Field",
			method: "POST",
			requestBody: http_handler.Transaction{
				CardDetails: http_handler.CardDetails{
					Number: "4539 3195 0343 6467",
					Expiry: "01/24",
					Cvv:    "",
				},
			},
			expectedBody: struct {
				Message    string
				StatusCode int
				Status     string
				MaskedCard string
				Error      string
				MerchatId  string
			}{
				Message:    "failed",
				StatusCode: 400,
				Error:      "enter valid cvv/cvc number",
				MerchatId:  "merchant-4564",
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			reqBody, _ := json.Marshal(tc.requestBody)
			req := httptest.NewRequest(tc.method, "/transaction", strings.NewReader(string(reqBody)))
			w := httptest.NewRecorder()

			payment := http_handler.Payment{}
			creditCard := http_handler.CreditCard{}
			handler := payment.Process(creditCard)
			handler(w, req)

			fmt.Println()
			fmt.Printf("request body %+v", tc.expectedBody)
			fmt.Println()

			res := w.Result()
			if res.StatusCode != tc.expectedBody.StatusCode {
				t.Fatalf("expected status %d, got %d", tc.expectedBody.StatusCode, res.StatusCode)
			}

			var response util.Response
			err := json.NewDecoder(res.Body).Decode(&response)
			if err != nil {
				t.Fatalf("error decoding response: %v", err)
			}

			if response.Message != tc.expectedBody.Message {
				t.Fatalf("expected message %q, got %q", tc.expectedBody.Message, response.Message)
			}

			if tc.name == "Valid Transaction" {
				result := response.Result.(map[string]interface{})
				cardDetails := result["cardDetails"].(map[string]interface{})
				if cardDetails["number"] != tc.expectedBody.MaskedCard {
					t.Fatalf("expected masked card number %q, got %q", tc.expectedBody.MaskedCard, cardDetails["number"])
				}
			}

			fmt.Printf("response body%+v", response)
			fmt.Println()

		})
	}
}
