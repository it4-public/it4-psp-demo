package util

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
	Result     any    `json:"result"`
	Error      string `json:"error"`
}

func SuccessResponse(w http.ResponseWriter, code int, message string, result any) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	SuccessResponse := Response{
		Message:    "successfull",
		StatusCode: code,
		Result:     result,
	}
	json.NewEncoder(w).Encode(SuccessResponse)
}

func FailureResponse(w http.ResponseWriter, code int, message, err string, result any) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	failureResponse := Response{
		Message:    message,
		StatusCode: code,
		Result:     result,
		Error:      err,
	}
	json.NewEncoder(w).Encode(failureResponse)
}
