package util

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// generateTransactionID creates a random string for the transaction ID
func GenerateTransactionID() string {
	rand.Seed(time.Now().UnixNano())
	return fmt.Sprintf("txn-%x", rand.Int63())
}

// validateCardNumber uses Luhn's algorithm to check card number validity (replace with a library for real implementation)
func ValidateCardNumber(cardNumber string) (bool, *NewError) {
	cardNumber = strings.ReplaceAll(cardNumber, " ", "")
	cardNumber = strings.ReplaceAll(cardNumber, "-", "")

	// Convert the card number into a slice of integers
	nums := make([]int, len(cardNumber))

	for i := len(cardNumber) - 1; i >= 0; i-- {
		digit, err := strconv.Atoi(string(cardNumber[i]))
		if err != nil {
			return false, &NewError{
				Message:    "failed",
				Error:      err.Error(),
				StatusCode: http.StatusInternalServerError,
			}
		}
		nums[len(cardNumber)-1-i] = digit
	}

	sum := 0
	for i := 0; i < len(nums); i++ {
		if i%2 == 1 {
			// Double every second digit from the right
			double := nums[i] * 2
			if double > 9 {
				// If the double is greater than 9, sum the digits (e.g., 14 becomes 1+4=5)
				double = double - 9
			}
			sum += double
		} else {
			// Otherwise, just add the digit
			sum += nums[i]
		}
	}

	// If the sum modulo 10 is 0, the card number is valid
	if sum%10 == 0 {
		return true, nil
	} else {
		return false, &NewError{
			Message:    "failed",
			Error:      "invalid card number",
			StatusCode: http.StatusBadRequest,
		}
	}
	//return sum%10 == 0, nil
}

// maskCardNumber replaces all digits except the last 4 with asterisks
func MaskCardNumber(cardNumber string) string {
	runes := []rune(cardNumber)
	for i := 0; i < len(runes)-4; i++ {
		runes[i] = '*'
	}
	return string(runes)
}

func CheckExpiredDate(expiryDateStr string) (bool, *NewError) {

	// dateParts Splits the input date into month and year
	dateParts := strings.Split(expiryDateStr, "/")
	if len(dateParts) != 2 {
		return false, &NewError{
			Message:    "failed",
			Error:      "invalid date format",
			StatusCode: http.StatusBadRequest,
		}
	}

	monthStr := dateParts[0]
	yearStr := dateParts[1]

	// Parse the month and year
	month, err := time.Parse("01", monthStr)
	if err != nil {
		return false, &NewError{
			Message:    "failed",
			Error:      err.Error(),
			StatusCode: http.StatusBadRequest,
		}
	}

	year, err := time.Parse("06", yearStr)
	if err != nil {
		return false, &NewError{
			Message:    "failed",
			Error:      err.Error(),
			StatusCode: http.StatusBadRequest,
		}
	}

	lastDayOfMonth := getLastDayOfMonth(year.Year(), int(month.Month()))

	expiryDate := time.Date(year.Year(), month.Month(), lastDayOfMonth.Day(), 23, 59, 59, 0, time.Now().UTC().Location())
	currentDate := time.Now()

	// Check if the expiry date is before the current date

	switch {
	case expiryDate.Before(currentDate):
		{
			return true, &NewError{
				Message:    "card is already expired",
				StatusCode: http.StatusBadRequest,
			}
		}
	default:
		return false, nil

	}

}

func getLastDayOfMonth(year int, month int) time.Time {
	// Create a date for the first day of the following month
	firstOfNextMonth := time.Date(year, time.Month(month)+1, 1, 0, 0, 0, 0, time.UTC)

	// Subtract a day to get the last day of the current month
	lastDayOfMonth := firstOfNextMonth.AddDate(0, 0, -1)

	return lastDayOfMonth
}
