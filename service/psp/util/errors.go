package util

import (
	"fmt"
	"net/http"
)

type Errors interface {
	ErrorMesesage() string
	FullError() string
}

type NewError struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
	Result     any    `json:"result"`
	Error      string `json:"error"`
}

func (e *NewError) ErrorMesesage() string {
	return e.Message
}

func (e *NewError) FullError() string {
	return fmt.Sprintf("error= %s message = %s status_code = %v",
		e.Error, e.Message, e.StatusCode)
}

func ErrorResponse(statuscode int, message, reason string) Errors {
	return &NewError{
		Message:    message,
		StatusCode: statuscode,
	}
}

func InternalServerError(message string) Errors {
	return &NewError{
		StatusCode: http.StatusInternalServerError,
		Message:    message,
	}
}

func NewNotfoundError(message string) Errors {
	return &NewError{
		StatusCode: http.StatusNotFound,
		Message:    message,
	}
}

func NewBadRequestError(message string) Errors {
	return &NewError{
		StatusCode: http.StatusBadRequest,
		Message:    message,
	}
}
