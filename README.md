# Payment Service Platform

The Payment Service Platform is a Go-based application designed for handling payment transactions, specifically focusing on credit card processing. It provides a simple HTTP API to process credit card transactions with logging and error handling capabilities.

## Table of Contents

Getting Started

Usage

Endpoints

Logging

Development

Contributing

License

## Getting Started

To get started with the Payment Service Platform, you'll need Go installed on your system. Follow these steps to set up the project:

Clone the repository:

git clone https://gitlab.com/it4-public/it4-psp-demo.git

Navigate to the project directory:

it4-public/service/psp/cmd/http/

Build the project:

go build -o pspdemo

Start the server:
./pspdemo

By default, the server listens on port 8780. You can change this in the main.go file or by setting an environment variable.

# Running the Application with Docker

To run the application using Docker, follow these steps:

## 1. Build the Docker Image

Build the Docker image from the provided Dockerfile:

docker build -t enter-image-name .

## 2. Run the Docker Container

docker run -p 8780:8780 enter-image-name

## Usage

To process a credit card transaction, send a POST request to the /transaction endpoint with the following JSON body structure:

json
{ 
"cardDetails":{  
"number": "5011054488597827",  
"expiry": "05/24",  
"cvv": "231"  
},
"status": "Pending",  
"amount": 100,  
"currency": "USD",
"merchantId": "merchant-123"  
}

## Endpoints

/transaction: Processes a credit card transaction. Expects a POST request with the correct JSON format.

## Logging

This project includes a simple logging middleware that logs the request URL and response status code to the terminal. This helps with debugging and monitoring.

## Development

If you're interested in contributing to this project, ensure you have a Go development environment set up. Use the following commands to run tests and ensure everything is functioning correctly:

Run tests:

Navigate to service/psp

Run : go test

## Contributing

Contributions are welcome! Please follow these guidelines to contribute:

Fork the repository.
Create a new branch for your feature or bugfix.
Make your changes and test them thoroughly.
Submit a pull request with a clear explanation of your changes.

## License

This project is licensed under the MIT License. See the LICENSE file for more information.
